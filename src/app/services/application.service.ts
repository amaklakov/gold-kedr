import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class ApplicationService {

	constructor(
		public spinner: NgxSpinnerService,
	) {
	}
}
