import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize } from 'rxjs/internal/operators';
import { ApplicationConst } from '../const/application.const';
import { ServicesConst } from '../const/services.const';
import { ApplicationService } from './application.service';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
	constructor(private appService: ApplicationService) {
	}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		if (!request.url.includes(ServicesConst.API_SHORT)) {
			return this.processRequest(next, request);
		}

		return this.makeRequest(localStorage.getItem(ApplicationConst.LOCALSTORAGE_SESSION_TOKEN), request, next);
	}

	private processRequest(next: HttpHandler, request: HttpRequest<any>) {
		this.appService.spinner.show();

		return next.handle(request).pipe(
			catchError((error: HttpErrorResponse) => {
				// this.showAlert(error);
				//
				// if (error.error === 401) {
				// 	this.appService.storageService.setItem(StorageItemsEnum.TOKEN, null);
				// }

				return throwError(error);
			}),
			finalize(() => this.appService.spinner.hide()),
		);
	}

	private makeRequest(token, request, next) {
		if (token) {
			token = `OAuth ${token}`;
		}

		const headers = {
			'Authorization': token || '',
			'Content-Type': 'application/json',
			'Accept': 'application/json',
		};

		console.log('====== request ->', request.url);

		request = request.clone({
			setHeaders: { ...headers },
			url: `${ServicesConst.BASE}/${request.url}`,
		});

		return this.processRequest(next, request);
	}
}
